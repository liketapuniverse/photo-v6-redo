//
//  ProjectDetail.swift
//  photo-redo-v6
//
//  Created by admin on 04/11/2021.
//

import UIKit

class Project {
    let id: String
    let name: String
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
