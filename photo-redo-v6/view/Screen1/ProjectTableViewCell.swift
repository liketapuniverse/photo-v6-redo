//
//  ProjectTableViewCell.swift
//  photo-redo-v6
//
//  Created by admin on 04/11/2021.
//

import UIKit
protocol ProjectTableViewCellDelegate: AnyObject {
    func deleteProject(_ cell: ProjectTableViewCell)
}

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var trailingConstant: NSLayoutConstraint!
    weak var delegate: ProjectTableViewCellDelegate?
    var didLoad = false
    var beginX: Double = 0
    
    override func draw(_ rect: CGRect) {
        if didLoad { return }
        if #available(iOS 14.0, *) {} else { deleteButton.setImage(UIImage(named: "minus30"), for: .normal)}
        trailingConstant.constant = 0
        let pan = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        pan.delegate = self
        addGestureRecognizer(pan)
        didLoad = true
    }
    
    @objc func didPan(_ g: UIPanGestureRecognizer) {
        if g.state == .began {
            beginX = trailingConstant.constant
        }
        if g.state == .changed {
            let translation = g.translation(in: self)
            trailingConstant.constant = beginX - translation.x
        }
        if g.state == .ended {
            var current = trailingConstant.constant
            current = max(0, current)
            current = min(50, current)
            current = current<20 ? 0: 50
            trailingConstant.constant = current
            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }
        }
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let p = gestureRecognizer as? UIPanGestureRecognizer else { return false}
        let v = p.velocity(in: self)
        return abs(v.x) > abs(v.y)
    }
    
    @IBAction func deleteProject(_ sender: Any) {
        if let d = delegate {
            d.deleteProject(self)
        }
    }
}
