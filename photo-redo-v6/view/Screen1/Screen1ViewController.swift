//
//  ViewController.swift
//  photo-redo-v6
//
//  Created by admin on 04/11/2021.
//

import UIKit

class Screen1ViewController: UIViewController {
    var model = ListProjects.shared
    @IBOutlet weak var projectsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(AppConstants.didLoadProjects), object: nil, queue: nil) { [weak self] _ in
            DispatchQueue.main.async {
                self?.projectsTableView.reloadData()
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func addProject(_ sender: Any) {
        let alert = UIAlertController(title: "Add project", message: "Enter project name", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Your project name here"
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            guard let textField = alert.textFields?.first else {return}
            if let txt = textField.text, !txt.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                let project = Project(id: UUID().uuidString, name: txt)
                self.model.projects.append(project)
                self.projectsTableView.insertRows(at: [IndexPath(row: self.model.projects.count - 1 , section: 0)], with: .automatic)
                let lastIndexPath = IndexPath(row: self.model.projects.count - 1, section: 0)
                self.projectsTableView.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
                self.model.saveProjectsList()
            } else {
                let emptyTextAlert = UIAlertController(title: "Empty text", message: "Please don't leave name blank", preferredStyle: .alert)
                emptyTextAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(emptyTextAlert, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension Screen1ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = projectsTableView.dequeueReusableCell(withIdentifier: "ProjectTableViewCell") as? ProjectTableViewCell else { return UITableViewCell()}
        cell.projectNameLabel.text =  model.projects[indexPath.row].name
        cell.delegate = self
        return cell
    }
    
    
}

extension Screen1ViewController: ProjectTableViewCellDelegate {
    func deleteProject(_ cell: ProjectTableViewCell) {
        if let indexPath = projectsTableView.indexPath(for: cell) {
            model.projects.remove(at: indexPath.row)
            projectsTableView.deleteRows(at: [indexPath], with: .fade)
            model.saveProjectsList()
        }
    }
}
