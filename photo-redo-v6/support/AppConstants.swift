//
//  AppConstants.swift
//  photo-redo-v6
//
//  Created by admin on 04/11/2021.
//

import Foundation

class AppConstants {
    static let endPoint = "https://tapuniverse.com"
    static let didLoadProjects = "didLoadProjects"
    static let projectsListFilename = "projectsList.json"
}
